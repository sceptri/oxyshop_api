<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Permissions;

class UserController extends AbstractController
{
	/**
	 * @Route("/user/get-all", name="get-all-users", methods={"GET"})
	 */
	public function getUsers(): Response
	{
		$users = $this->getDoctrine()->getRepository(User::class)->findAll();

		//Users given by the User Repository are in their object form, so we convert it into associative arrays
		$usersArray = array_map(function ($user) {
			return $user->toArray();
		}, $users);

		return new Response(json_encode($usersArray), 200);	
	}

	  /**
	 * @Route("/user/register", name="register-user", methods={"POST"})
	 */
	public function registerUser(Request $request, ValidatorInterface $validator): Response
	{
		//User manager used for managing users in DB
		$userManager = $this->getDoctrine()->getManager();

		$user = new User();

		//Requested data is supposed to be in JSON form
		$requestData = $request->toArray();
		
		$user->setName($requestData["name"]);
		//Email format and uniqueness is checked in User.php with Assert
		$user->setEmail($requestData["email"]);
		//Hash password with the default PHP password hashing function (probably bcrypt) - more secure than SHA256 or other encryption algos
		$user->setPassword(password_hash($requestData["password"], PASSWORD_DEFAULT));

		//If permissions is defined and has valid value
		if(array_key_exists("permissions", $requestData)) {
			if($requestData["permissions"] == Permissions::User || $requestData["permissions"] == Permissions::Admin) {
				$user->setPermissions($requestData["permissions"]);
			} else {
				return new Response("Permissions field has incorrect value: ".$requestData["permissions"], 400);
			}
		}

		$errors = $validator->validate($user);
		if(count($errors) > 0) {
			// Return string of errors for nicer debugging
			$errorsString = print_r($errors, true);

			return new Response($errorsString, 400);
		}

		//Put $user into user manager queue
		$userManager->persist($user);

		//We are only putting 1 user to DB so we "apply the changes"
		$userManager->flush();

		return new Response($user->getId());
	}
}
