# oXyshop interview - api part
Provides REST API for simple user registration. It uses oxyshop_frontend (https://gitlab.com/sceptri/oxyshop_frontend) to display users and provide user registration form.

Needed software:
```
PHP 7.2.5 or higher
Composer
Mariadb or MySQL or other
```


## Installing
Copy the `oxyshop_api` to location of your choosing.

Start mysql or mariadb service (for example `systemctl start mysql`)

Create database for `oxyshop_frontend`.

> You will need to create database for *both* oxyshop_api and oxyshop_frontend

Open `.env` and change `DATABASE_URL` to your server, port, username, password and database name.

Migrate with `php bin/console doctrine:migration:migrate`.

Open terminal in this folder and run `composer require symfony/runtime` and then start server with `symfony server:start`.

Follow instructions at `oxyshop_frontpage`.

Enjoy!
